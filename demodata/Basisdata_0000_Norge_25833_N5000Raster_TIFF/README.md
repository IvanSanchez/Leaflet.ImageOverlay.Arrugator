This raster image is the N5000 raster dataset from Kartverket (Norway's mapping authority). Karverket offers this data for download, under a CC-by-4.0 license (https://creativecommons.org/licenses/by/4.0/)

For more information about this dataset, visit:

https://kartkatalog.geonorge.no/metadata/n5000-raster/7e1b827e-3ccd-48a8-91ce-cc8125d6e4c1
